$(function() {
  //slider
  (function workSliders() {
    // menu fixed
    $(window).bind('scroll', function () {
      if ($(window).scrollTop() > $('header').height()) {
        $('main').css('padding-top', $('header').height())
          $(document.body).addClass('fixed');
      } else {
          $(document.body).removeClass('fixed');
          $('main').css('padding-top', '')
      }
    });
    // slider-project
    if($().slick) {
      function initSlider(slider, slides, slidesActive, progressBar) {
        var stickWidth = 100 / (+slides.length - slidesActive.length)
        function changeSlideSlick() {
          slides.each(function(i) {
            if ($(this).hasClass('slick-current')) {
              progressBar.css('width', stickWidth * (i) + '%') 
            }
          })
        }
        slider.on('beforeChange, afterChange, setPosition', function() {
          changeSlideSlick()
        })
        changeSlideSlick()
      }

      if($('#sliderProject')) {
        var sliderProject =  $('#sliderProject').slick({
          infinite: false,
          slidesToShow: 4,
          slidesToScroll: 1,
          prevArrow: $("#projectPrev"),
          nextArrow: $("#projectNext"),
          speed: 200,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,

              }
            },
            {
              breakpoint: 995,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });
        initSlider(sliderProject, $('#sliderProject .slick-slide'), $('#sliderProject .slick-slide.slick-active'), $('#progressProject'))
      } 

      if($('#sliderNews')) {
        var sliderNews = $('#sliderNews').slick({
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: false,
          prevArrow: $("#newsPrev"),
          nextArrow: $("#newsNext"),
          speed: 200,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,

              }
            },
            {
              breakpoint: 995,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
          
        })
        initSlider(sliderNews, $('#sliderNews .slick-slide'), $('#sliderNews .slick-slide.slick-active'), $('#progressNews'))
      }

      //main slider
      if($('#mainSlider')) {
        var mainSlider = $('#mainSlider').slick({
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            speed: 200,
                            infinite: false,
                            dots: false
                          }),
          slidesMainSlider = $('#mainSlider .slick-slide')

        function changeActiveDotsMainSlider() {
          $('#mainDots span').each(function() {
            $(this).removeClass('active')
          })
          $('#mainSlider .slick-slide').each(function(i) {
            if($(this).hasClass('slick-active')) {
              $('#mainDots span')[i].classList.add('active')
            }
          })
        }

        slidesMainSlider.each(function(i) {
          if (i > 9) {
            $('#mainDots').append('<span data-index="'+ i +'">' + (i + 1) + '</span>')
          } else {
            $('#mainDots').append('<span data-index="'+ i +'">0' + (i + 1) + '</span>')
          }
        })

        $('#mainDots').on('click', function(event) {
          if(event.target.tagName = 'SPAN') {
            mainSlider.slick('slickGoTo', +$(event.target).attr('data-index'))
            changeActiveDotsMainSlider()
          }
        })

        mainSlider.on('beforeChange, afterChange, setPosition', function() {
          changeActiveDotsMainSlider()
        })

        changeActiveDotsMainSlider()
      } 

      //work company-slider
      if($('#companySlider')) {
        var companyBlock  = $('#company'),
            companySlider = $('#companySlider').slick({
                              slidesToShow: 1,
                              slidesToScroll: 1,
                              speed: 200,
                              infinite: false,
                              dots: false,
                              prevArrow: $("#sliderCompanySlidePrev"),
                              nextArrow: $("#sliderCompanySlideNext"),
                            });
              

        function changeWidthProgressBarCompanySlider(elem, range) {
          range.css('width', elem.offset().left + 'px')
        }

        function changeDirectionMessageCompanySlider(elem, offsetLeft) {
          if (offsetLeft > $(window).innerWidth() / 2) {
            elem.addClass('left')
          } else {
            elem.addClass('right')
          }
        } 

        function changeActiveDotsComanySlider() {
          $('.company-slider__range .company-slider__dot').each(function(i) {
            if(i === companySlider.slick('slickCurrentSlide')) {
              $(this).addClass('active')
              changeWidthProgressBarCompanySlider($(this), $('#progressCompanySlider'))
            } else {
              $(this).removeClass('active')
            }
            changeDirectionMessageCompanySlider($(this).children('.company-slider__dot_message.message'), $(this).offset().left)
          })
        }

        companySlider.on('beforeChange, afterChange, setPosition', function() {
          changeActiveDotsComanySlider()
        })

        companyBlock.on('click', function(event) {
          if(event.target.tagName === 'DIV' && $(event.target).hasClass('company-slider__dot')) {
            companySlider.slick('slickGoTo', +$(event.target).attr('data-index'))
            changeActiveDotsComanySlider()
          }
        })
        changeActiveDotsComanySlider()
      }
    } 
  }());

  // gallery in page production
  (function workGallery() {
    var gallery = $('#gallery')
    var galleryItems = $('#gallery .gallery__grid .gallery__item')
    var imageMain = $('#gallery .gallery__hero img')
    
    function changeImageMain(galleryItems) {
      if(galleryItems) {
        galleryItems.each(function() {
          if($(this).hasClass('active')) imageMain.attr('src', $(this).children('img').attr('src'))
        })
      } else console.log(undefined);
    }

    gallery.on('click', function(event) {
      if(event.target.closest('.gallery__item img')) {
        galleryItems.each(function(){$(this).removeClass('active')})
        $(event.target).parent('.gallery__item').addClass('active')
        changeImageMain(galleryItems)
      }
    })

    changeImageMain(galleryItems);
  }());
  
  (function dropMenuHeader() {
    $('.header .header__nav .header__drop-menu').click(function() {
      $(document.body).toggleClass('show-panel');
    })
  }());

  //tabs in page production
  (function workTabs() {
    var tabsInformation = $('#tabInformation button')
    var tabBlockInformation = $('#tabInformation')
    var tabInformationItems = $('.product-information__wrap .product-information__item')

    function hideTabInformationItems(index) {
      tabsInformation.each(function(i) {
        if (index === i) {
          $(this).addClass('active')
        } else $(this).removeClass('active')
      })
      tabInformationItems.each(function(i) {
        if (index !== i) {
          $(this).css('display', 'none')
        } else {
          $(this).css('display', '')
        }
      })
    }

    tabBlockInformation.on('click', function(event) {
      tabsInformation.each(function(i) {
        if ($(this)[0] === event.target) hideTabInformationItems(i)
      })
    })
    
    hideTabInformationItems(0)
  }());
  
  (function() {
    $('.custom-select').each(function() {
      var str,
      $cSelect = $(this).children('.custom-select__select'),
      select = $(this)

      str = getVal($(this));
      select.children('.custom-select__selected-text').text(str);
      
      $cSelect.on('change', function() {
        str = getVal($(this));
        select.children('.custom-select__selected-text').text(str);
        
        $(this).blur();
      });
    })
    
    function getVal(select) {
      return select.find('option:selected').text()
    }
  }());

  if($('.video a')) {
    $('.video a').each(function() {
      console.log($(this).fancybox());
    })
  }

  if(document.getElementById("map")) {
    

    // function initialize() {     
    //   var myLatlng = new google.maps.LatLng(56.484680, 84.948197);
    //   var myOptions = {
    //     zoom: 10,
    //     center: myLatlng,
    //     mapTypeId: google.maps.MapTypeId.ROADMAP,
    //     styles:[
    //       {
    //         "elementType": "geometry",
    //         "stylers": [
    //           {
    //             "color": "#ffffff"
    //           }
    //         ]
    //       },
    //       {
    //         "elementType": "geometry.fill",
    //         "stylers": [
    //           {
    //             "color": "#ffffff"
    //           }
    //         ]
    //       },
    //       {
    //         "elementType": "geometry.stroke",
    //         "stylers": [
    //           {
    //             "color": "#000000"
    //           }
    //         ]
    //       },
    //       {
    //         "elementType": "labels.icon",
    //         "stylers": [
    //           {
    //             "visibility": "off"
    //           }
    //         ]
    //       },
    //       {
    //         "elementType": "labels.text.fill",
    //         "stylers": [
    //           {
    //             "color": "#616161"
    //           }
    //         ]
    //       },
    //       {
    //         "elementType": "labels.text.stroke",
    //         "stylers": [
    //           {
    //             "color": "#f5f5f5"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "administrative.country",
    //         "elementType": "geometry.fill",
    //         "stylers": [
    //           {
    //             "visibility": "on"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "administrative.country",
    //         "elementType": "geometry.stroke",
    //         "stylers": [
    //           {
    //             "color": "#7d7d7d"
    //           },
    //           {
    //             "weight": 1
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "administrative.land_parcel",
    //         "elementType": "labels.text.fill",
    //         "stylers": [
    //           {
    //             "color": "#bdbdbd"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "administrative.locality",
    //         "elementType": "geometry.stroke",
    //         "stylers": [
    //           {
    //             "visibility": "on"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "administrative.province",
    //         "elementType": "geometry",
    //         "stylers": [
    //           {
    //             "visibility": "on"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "administrative.province",
    //         "elementType": "geometry.stroke",
    //         "stylers": [
    //           {
    //             "color": "#616161"
    //           },
    //           {
    //             "visibility": "on"
    //           },
    //           {
    //             "weight": 1
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "poi",
    //         "elementType": "geometry",
    //         "stylers": [
    //           {
    //             "color": "#eeeeee"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "poi",
    //         "elementType": "labels.text.fill",
    //         "stylers": [
    //           {
    //             "color": "#757575"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "poi.park",
    //         "elementType": "geometry",
    //         "stylers": [
    //           {
    //             "color": "#e5e5e5"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "poi.park",
    //         "elementType": "labels.text.fill",
    //         "stylers": [
    //           {
    //             "color": "#9e9e9e"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "road",
    //         "elementType": "geometry",
    //         "stylers": [
    //           {
    //             "color": "#ffffff"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "road.arterial",
    //         "elementType": "labels.text.fill",
    //         "stylers": [
    //           {
    //             "color": "#757575"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "road.highway",
    //         "elementType": "geometry",
    //         "stylers": [
    //           {
    //             "color": "#dadada"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "road.highway",
    //         "elementType": "labels.text.fill",
    //         "stylers": [
    //           {
    //             "color": "#616161"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "road.local",
    //         "elementType": "labels.text.fill",
    //         "stylers": [
    //           {
    //             "color": "#9e9e9e"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "transit.line",
    //         "elementType": "geometry",
    //         "stylers": [
    //           {
    //             "color": "#e5e5e5"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "transit.station",
    //         "elementType": "geometry",
    //         "stylers": [
    //           {
    //             "color": "#eeeeee"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "water",
    //         "elementType": "geometry",
    //         "stylers": [
    //           {
    //             "color": "#c9c9c9"
    //           }
    //         ]
    //       },
    //       {
    //         "featureType": "water",
    //         "elementType": "labels.text.fill",
    //         "stylers": [
    //           {
    //             "color": "#9e9e9e"
    //           }
    //         ]
    //       }
    //     ]
    //     }
    //     var map = new google.maps.Map(document.getElementById("map"), myOptions); 
    //   }
    //   initialize()
  }

  (function maskPhone() {
    $('.checkbox').each(function() {
      var checkbox = $(this)
     $(this).children('input').on('change', function(event) {
        if(event.target.checked) {
          checkbox.removeClass('error')
        }
      });
    })
    $('input[type="tel"').each(function() {
      $(this).mask('+7 (000) 000-00-00')
    })
  }());
  
    
  (function maskPhone(){
    $('input').on('input, blur, change', function(event) {
      if(event.target.tagName === 'INPUT') {
        if(event.target.value) {
          $(event.target).addClass('active')
        } else {
          $(event.target).removeClass("active")
        }
      }
    })

  }());
      
   
})